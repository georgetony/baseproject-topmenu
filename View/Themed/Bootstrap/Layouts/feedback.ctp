<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $title_for_layout; ?>
	</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php
		// css/style.css
		echo $this->Html->css('style');
		// Bootstrap CSS
		echo $this->Html->css('bootstrap');
		echo $this->Html->css('bootstrap-theme');

		// latest jquery
		echo $this->Html->script('jquery-1.10.2.min.js', false);
		// Bootstrap JS
		echo $this->Html->script('bootstrap', false);
		// js/custom.js
		echo $this->Html->script('custom', false);

	?>
</head>
<body>
	<div class='container contents'>
		<?php echo $this->Session->flash(); ?>

		<?php echo $this->fetch('content'); ?>
	</div>

	<div id="footer">
	</div>
</div>
<?php
	// This will output the cached js into this position.
	// Refer to Js helper in CakePHP 2.x
	echo $this->fetch('css');
	echo $this->fetch('script');
?>
</body>
</html>
