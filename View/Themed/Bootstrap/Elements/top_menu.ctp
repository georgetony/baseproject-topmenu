<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<?php
				echo $this->Html->link('Base Project', '/', array(
					'class' => 'navbar-brand'
				));
			?>
		</div>
		<div class="collapse navbar-collapse">
			<ul class="nav navbar-nav">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Users<b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li>
						<?php
						echo $this->Html->link(
							'List',
							array('controller' => 'users', 'action' => 'index'),
							array('escape' => false, 'class' => '')
						);

						?>
						</li>
						<li>
						<?php
						echo $this->Html->link(
							'Create New',
							array('controller' => 'users', 'action' => 'add'),
							array('escape' => false, 'class' => '')
						);

						?>
						</li>
					</ul>
				</li>
				<li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Roles<b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li>
						<?php
						echo $this->Html->link(
							'List',
							array('controller' => 'roles', 'action' => 'index'),
							array('escape' => false, 'class' => '')
						);

						?>
						</li>
						<li>
						<?php
						echo $this->Html->link(
							'Create New',
							array('controller' => 'roles', 'action' => 'add'),
							array('escape' => false, 'class' => '')
						);

						?>
						</li>
					</ul>
				</li>
				<li>
				<?php
				echo $this->Html->link(
					'Logout',
					array('controller' => 'users', 'action' => 'logout'),
					array('escape' => false, 'class' => '')
				);
				?>
				</li>
			</ul>
		</div><!--/.nav-collapse -->
	</div>
</div>
