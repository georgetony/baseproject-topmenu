<p>
	<?php
		echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
		));
	?>
</p>

<div class="paging">
	<?php
		echo $this->Paginator->prev(__('< Previous'), array('class' => 'btn prev'), null, array('class' => 'prev disabled btn'));
		echo $this->Paginator->numbers(array('separator' => '&nbsp;&nbsp;', 'first' => 2, 'last' => 2));
		echo $this->Paginator->next(__('Next >'), array('class' => 'btn prev'), null, array('class' => 'next disabled btn'));
	?>
</div>

