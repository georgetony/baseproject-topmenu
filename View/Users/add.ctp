<?php
echo $this->Form->create('User', array(
	'class' => 'form',
	'inputDefaults' => array(
		'div' => 'form-group',
		//'after' => '</div>',
		'class' => 'form-control',
		'error' => array(
			'attributes' => array('wrap' => 'div', 'class' => 'validation-error-msg')
		)
		)
));
?>
	<h3>Create New User</h3>
		<?php
			// Hidden fields
			echo $this->Form->input('role_id', array(
				'type' => 'hidden',
				'value' => '1'
			));
		?>
		<?php
			echo $this->Form->input('name', array(
				'type' => 'text',
				'placeholder' => 'Name of the user',
				'label' => array(
					'text' => 'Full Name'
				)
			));
		?>
		<?php
			echo $this->Form->input('username', array(
				'type' => 'text',
				'placeholder' => 'Username',
				'label' => array(
					'text' => 'Username (unique)'
				)
			));
		?>
		<?php
			echo $this->Form->input('password', array(
				'type' => 'password',
				'placeholder' => 'Password',
				'label' => array(
					'text' => 'Password'
				)
			));
		?>
		<?php
			echo $this->Form->input('confirm_password', array(
				'type' => 'password',
				'placeholder' => 'Enter Password Again',
				'label' => array(
					'text' => 'Confirm Password'
				)
			));
		?>
	<button type="submit" class="btn btn-success">Submit</button>
<?php
echo $this->Form->end();
?>
