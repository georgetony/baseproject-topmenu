<?php
echo $this->Form->create('User', array(
	'class' => 'form',
	'inputDefaults' => array(
		'div' => 'form-group',
		//'after' => '</div>',
		'class' => 'form-control',
		'error' => array(
			'attributes' => array('wrap' => 'div', 'class' => 'validation-error-msg')
		)
		)
));
?>
	<h3>Edit User: <?php echo $this->data['User']['name']; ?></h3>
		<?php
			// Hidden fields
			echo $this->Form->input('id', array(
				'type' => 'hidden'
			));
			echo $this->Form->input('role_id', array(
				'type' => 'hidden',
				'value' => '1'
			));
		?>
		<?php
			echo $this->Form->input('name', array(
				'type' => 'text',
				'placeholder' => 'Name of the user',
				'label' => array(
					'text' => 'Full Name'
				)
			));
		?>
	<button type="submit" class="btn btn-success">Submit</button>
<?php
echo $this->Form->end();
?>
