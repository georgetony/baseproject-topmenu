<?php echo $this->Html->css('login'); ?>
<?php echo $this->Form->create('User', array('class' => 'form-signin')); ?>
	<h3 class="form-signin-heading">Login</h3>
<?php
	echo $this->Form->input('username', array(
		'type' => 'text',
		'class' => 'form-control',
		'placeholder' => 'username',
		'required',
		'autofocus',
		'label' => false
	));
	echo $this->Form->input('password', array(
		'type' => 'password',
		'class' => 'form-control',
		'placeholder' => 'password',
		'required',
		'autofocus',
		'label' => false
	));
?>
<div class="clearfix visible-xs"></div>
<br>
<button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
<?php
	echo $this->Form->end();
?>
