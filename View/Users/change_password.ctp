<?php
echo $this->Form->create('User', array(
	'class' => 'form',
	'inputDefaults' => array(
		'div' => 'form-group',
		//'after' => '</div>',
		'class' => 'form-control',
		'error' => array(
			'attributes' => array('wrap' => 'div', 'class' => 'validation-error-msg')
		)
		)
));
?>
	<h3>Change Password: <?php echo $user['User']['name']?></h3>
		<?php
			// Hidden fields
			echo $this->Form->input('id', array(
				'type' => 'hidden',
				'value' => $user['User']['id']

			));
		?>
		<?php
			echo $this->Form->input('new_password', array(
				'type' => 'password',
				'placeholder' => 'New Password',
				'label' => array(
					'text' => 'New Password'
				)
			));
		?>
		<?php
			echo $this->Form->input('confirm_password', array(
				'type' => 'password',
				'placeholder' => 'Enter Password Again',
				'label' => array(
					'text' => 'Confirm Password'
				)
			));
		?>
	<button type="submit" class="btn btn-success">Submit</button>
<?php
echo $this->Form->end();
?>
