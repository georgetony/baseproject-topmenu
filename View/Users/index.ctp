<div class="table-responsive">
	<h3><?php echo __('Users'); ?></h3>
	<table cellpadding="0" cellspacing="0" class="table">
	<tr>
			<th><?php echo '#'; ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('username'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($users as $user): ?>
	<tr>
		<td><?php echo h($user['User']['id']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['name']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['username']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link('<button type="button" class="btn btn-success btn-xs">Edit User</button>', array('action' => 'edit', $user['User']['id']), array('escape' => false)); ?>
			<?php echo $this->Html->link('<button type="button" class="btn btn-success btn-xs">Change Password</button>', array('action' => 'change_password', $user['User']['id']), array('escape' => false)); ?>
			<?php //echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $user['User']['id']), null, __('Are you sure you want to delete # %s?', $user['User']['id'])); ?>
		</td>
	</tr>
	<?php endforeach; ?>
	</table>
	<?php echo $this->element('pagination'); ?>
</div>
