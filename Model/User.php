<?php
App::uses('AppModel', 'Model');
/**
 * User Model
 *
 * @property Role $Role
 */
class User extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'role_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'This is a required field',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'username' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'This is a required field.',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'unique' => array(
				'rule' => array('isUnique'),
				'message' => 'This username exists.',
				//'allowEmpty' => false,
				//'required' => 'create',
				//'last' => false, // Stop validation after this rule
				'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'password' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'minlength' => array(
				'rule' => array('minlength', 6),
				'message' => 'Min 6 characters.',
			),
		),
		'new_password' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'This is a required field',
			),
			'minlength' => array(
				'rule' => array('minlength', 6),
				'message' => 'Min 6 characters.',
			),
		),

		'confirm_password' => array(
			'rule' => 'confirmPassword',
			'message' => 'Passwords don\'t match. Please try again',
		)
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Role' => array(
			'className' => 'Role',
			'foreignKey' => 'role_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	/**
	 * BeforeSave method.
	 */

	public function beforeSave($options = array()) {
		if (isset($this->data['User']['password'])) {
			$this->data['User']['password'] = AuthComponent::password(
				$this->data['User']['password']
			);
		}

		// Check for password change.
		if(isset($this->data['User']['new_password'])){
			$this->data['User']['password'] = AuthComponent::password(
				$this->data['User']['new_password']
			);
		}

		return true;
	}

	/**
		Confirm password. Check if both passwords are the same
	 */
	public function confirmPassword($data){
		
		if (isset($this->data['User']['password'])) {
			if($this->data['User']['password'] == $this->data['User']['confirm_password']){
				return true;
			}
		}

		if($this->data['User']['new_password'] == $this->data['User']['confirm_password']){
			return true;
		}
		return false;
	}

}
